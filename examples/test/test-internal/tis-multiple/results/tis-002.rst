TIS 1D example
==============

Simulation settings
-------------------
task = 'tis'
steps = 50
interfaces = [-0.9, -0.6, 1.0]
zero_ensemble = False
seed = 16
rgen = 'rgen-borg'
priority_shooting = False
flux = False

System settings
---------------
units = 'reduced'
dimensions = 1
temperature = 0.7
rgen = 'rgen-borg'

Engine settings
---------------
class = 'Langevin'
timestep = 0.05
gamma = 0.3
high_friction = False
seed = 16
rgen = 'rgen'
type = 'internal'

Box settings
------------
periodic = [False]

Particles settings
------------------
position = {'input_file': 'initial.xyz'}
velocity = {'generate': 'maxwell',
            'momentum': False,
            'rgen': 'rgen',
            'seed': 0}
mass = {'Ar': 1.0}
name = ['Ar']
ptype = [0]
type = 'internal'

Forcefield settings
-------------------
description = '1D double well'

Potential
---------
class = 'DoubleWell'
a = 1.0
b = 2.0
c = 0.0

Orderparameter settings
-----------------------
class = 'Position'
dim = 'x'
index = 0
periodic = False
name = 'Order Parameter'

Output settings
---------------
backup = 'overwrite'
energy-file = 100
order-file = 100
restart-file = 10
trajectory-file = 100
cross-file = 1
pathensemble-file = 1
screen = 10

TIS settings
------------
freq = 0.5
maxlength = 20000
aimless = True
allowmaxlength = False
zero_momentum = False
rescale_energy = False
sigma_v = -1
seed = 16
rgen = 'rgen-borg'
high_accept = False
shooting_move = 'sh'
shooting_moves = []
mirror_freq = 0
target_freq = 0
target_indices = []
ensemble_number = 2
detect = -0.3

Initial-path settings
---------------------
method = 'kick'

Analysis settings
-----------------
blockskip = 1
bins = 100
maxblock = 1000
maxordermsd = -1
ngrid = 1001
plot = {'output': 'png', 'plotter': 'mpl', 'style': 'pyretis'}
report = ['latex', 'rst', 'html']
skipcross = 1000
txt-output = 'txt.gz'
tau_ref_bin = []
skip = 0
