##############################
PyRETIS - TIS analysis
##############################


Please note that the flux (and thus the rate constant)
was **not** calculated in this analysis!

The main result is:

* The crossing probability:
  :math:`P_{\text{cross}} = 9.719267139e-02  \pm  35.215475032 \%`

.. _combined-results:

Combined results
================

The overall matched probability distributions are shown in the left figure
while the matched probability distribution is shown in the right figure below.
The overall crossing probability as a function of cycles
and its relative error block analysis are reported in the two following
plots, respectively.

.. image:: total-probability.png
   :width: 45%
.. image:: matched-probability.png
   :width: 45%

.. image:: overall-prun.png
   :width: 45%
.. image:: overall-err.png
   :width: 45%

Detailed results are given below for the different path
ensembles.

.. _figure-results:

Results for path ensembles
==========================

The following interfaces were used in the simulation and in
the analysis:

+-------------------------------------------+
|Interfaces                                 |
+----------+----------+----------+----------+
| Ensemble |   Left   |  Middle  |  Detect  |
+==========+==========+==========+==========+
|  [0^+]   | -0.9000  | -0.9000  | -0.6000  |
+----------+----------+----------+----------+
|  [1^+]   | -0.9000  | -0.6000  | -0.3000  |
+----------+----------+----------+----------+
|  [2^+]   | -0.9000  | -0.3000  |  1.0000  |
+----------+----------+----------+----------+

+-----------------------------------------------------+
|Crossing probabilities                               |
+----------+------------+------------+----------------+
| Ensemble |   Pcross   |   Error    | Rel. error (%) |
+==========+============+============+================+
|  [0^+]   |  0.244444  |  0.041023  |   16.782091    |
+----------+------------+------------+----------------+
|  [1^+]   |  0.812500  |  0.054951  |    6.763221    |
+----------+------------+------------+----------------+
|  [2^+]   |  0.489362  |  0.147845  |   30.211752    |
+----------+------------+------------+----------------+

+-------------------------------------------------------------------------------+
|Pathensemble data                                                              |
+----------+------------+------------------+-----------------+------------------+
| Ensemble | TIS cycles | Shoot acc. ratio | Swap acc. ratio | Avg. path length |
+==========+============+==================+=================+==================+
|  [0^+]   |     45     |     0.755556     |      nan        |    25.066667     |
+----------+------------+------------------+-----------------+------------------+
|  [1^+]   |     48     |     0.395833     |      nan        |    47.333333     |
+----------+------------+------------------+-----------------+------------------+
|  [2^+]   |     47     |     0.468085     |      nan        |    45.829787     |
+----------+------------+------------------+-----------------+------------------+

The crossing probabilities are also displayed in the figures below

.. _prob-figures-output:

Crossing probabilities
----------------------


.. image:: 001_pcross.png
   :width: 30%
.. image:: 001_prun.png
   :width: 30%
.. image:: 001_perror.png
   :width: 30%

.. image:: 002_pcross.png
   :width: 30%
.. image:: 002_prun.png
   :width: 30%
.. image:: 002_perror.png
   :width: 30%

.. image:: 003_pcross.png
   :width: 30%
.. image:: 003_prun.png
   :width: 30%
.. image:: 003_perror.png
   :width: 30%



.. _len-shoot-figures-output:

Distributions for path lengths and shooting moves
-------------------------------------------------

The average path lengths in the different ensembles are given in
the table below and some distributions for the path lengths and
shooting moves can also be found here:


.. image:: 001_lpath.png
   :width: 45%
.. image:: 001_shoots.png
   :width: 45%

.. image:: 002_lpath.png
   :width: 45%
.. image:: 002_shoots.png
   :width: 45%

.. image:: 003_lpath.png
   :width: 45%
.. image:: 003_shoots.png
   :width: 45%



