test-modify-velocities
======================

This test is intended to check that we can draw random
velocities using LAMMPS.

The test is run using:

python test_lammps.py
