test-propagate
==============

This will check if we can propagate the equations of motion
correctly using LAMMPS.

The test is run using:

python test_lammps.py

Graphs, showing the output of the test can be produced by running:

python test_lammps.py plot
