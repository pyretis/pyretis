# -*- coding: utf-8 -*-
# Copyright (c) 2023, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""This module just contains some info for PyRETIS.

Here we define the name of the program and some other
relevant info.

"""
PROGRAM_NAME = 'PyVisA'
URL = 'http://www.pyretis.org'
GIT_URL = 'https://gitlab.com/pyretis/pyretis'
CITE = """
[1] E. Riccardi, A. Lervik, S. Roet, O. Aarøen and T. S. van Erp,
    J. Comput. Chem., 2019, doi: https://dx.doi.org/10.1002/jcc.26112
[2] O. Aarøen, H. Kiær, E. Riccardi,
    J. Comput. Chem., 2020, doi: https://dx.doi.org/10.1002/jcc.26467
"""
LOGO = r"""
     _______
    /   ___ \        __   _ _       ___
   /_/\ \_/ /_  __  | |  / (_)___  /   |
      /  __  / / /  | | / / / __/ / /| |
     / /   \ \/ /   | |/ / /\ \  / ___ |
    /_/    _\  /    |___/_/___/ /_/  |_|
          /___/
      ,       ,       ,       ,       ,
    .'|     .'|     .'|     .'|     .'|
    |*****  | :     | :     | :     | :
    : '   **: '     : '     : '     : '
    | |     |**  ***|****   | |     | |
    ' :     ' :**   ' :  ***'*:     ' :
    ; |     ; |     ; |     ; ***** ; |
    | :     | :  *  | :     | :    *| :
    : '     :*** ***: '     : ' *** : '
    | |  ***| | *   |*******|***    | |
    :****   : ;     : ;     : ;     : ;
    ,/      ,/      ,/      ,/      ,/
    '       '       '       '       '

"""
