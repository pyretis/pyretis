Simulation
----------
task = RETIS
steps = 10
interfaces = [-0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, 1.0]

OrderParameter
-------------
name = 'Uno'
class = Position

Collective-variable
-------------------
name = 'Nessuno'
class = 'unicorn'

Collective-variable
-------------------
name = 'Centomila'
